module.exports = {
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "Skyward";
      return args;
    });
  },
  pwa: {
    name: "Skyward",
    appleMobileWebAppStatusBarStyle: "black",
  },
};

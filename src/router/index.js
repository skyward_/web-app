import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Register from "../views/Register.vue";
import Login from "../views/Login.vue";
import Faq from "../views/Faq.vue";
import Contact from "../views/Contact.vue";
import Statistique from "../views/Statistique.vue";
import About from "../views/About.vue";
import Price from "../views/Price.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/faq",
    name: "Faq",
    component: Faq,
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
  },
  {
    path: "/dashboard",
    name: "Statistique",
    component: Statistique,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/price",
    name: "Price",
    component: Price,
  },
  {
    path: "/map",
    name: "Map",
    component: function() {
      return import("../views/Map.vue");
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
